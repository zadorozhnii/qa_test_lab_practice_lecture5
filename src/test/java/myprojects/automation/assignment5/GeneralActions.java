package myprojects.automation.assignment5;


import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.model.UserData;
import myprojects.automation.assignment5.utils.DataConverter;
import myprojects.automation.assignment5.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private static By allProductsSelector = By.cssSelector(".all-product-link.pull-xs-left.pull-md-right.h4");
    private static By listAllProductsSelector = By.xpath("//div[@class = 'thumbnail-container']/a");
    private static By addToCartButtonSelector = By.cssSelector(".btn.btn-primary.add-to-cart");
    private static By proceedPurchaseButtonSelector = By.xpath("//*[@class = 'modal-dialog']//a");
    private static By productNameSelector = By.cssSelector(".h1");
    private static By productPriceSelector = By.xpath("//div[@class = 'current-price']/span[1]");
    private static By productDetailSelector = By.xpath("//a[@href= '#product-details']");
    private static By productQtySelector = By.xpath("//*[@class = 'product-quantities']//span");
    private static By productNameInCartSelector = By.xpath("//*[@class = 'product-image media-middle']/img");
    private static By productQtyInCartSelector = By.cssSelector(".js-cart-line-product-quantity.form-control");
    private static By productPriceInCartSelector = By.className("product-price");
    private static By createOrderButtonSelector = By.cssSelector(".btn.btn-primary");
    private static By inputUserNameSelector = By.name("firstname");
    private static By inputUserLastNameSelector = By.name("lastname");
    private static By inputUserEmailSelector = By.name("email");
    private static By continueButtonSelector = By.name("continue");
    private static By inputUserAddressSelector = By.name("address1");
    private static By inputUserZipSelector = By.name("postcode");
    private static By inputUserCitySelector = By.name("city");
    private static By confirmAddressButtonSelector = By.name("confirm-addresses");
    private static By paymentMethodCheckboxSelector = By.id("payment-option-1");
    private static By confirmCarrierSelector = By.name("confirmDeliveryOption");
    private static By termsConditionsSelector = By.id("conditions_to_approve[terms-and-conditions]");
    private static By finishOrderCreationButtonSelector = By.cssSelector(".btn.btn-primary.center-block");
    private static By productQtyInCartOnConfirmPageSelector = By.cssSelector(".col-xs-2");
    private static By productPriceInCartOnConfirmPageSelector = By.cssSelector(".col-xs-5.text-sm-right.text-xs-left");
    public static String successMessage = "ВАШ ЗАКАЗ ПОДТВЕРЖДЁН";


    public GeneralActions(WebDriver driver) {
        this.driver = driver;
    }

    public void openRandomProduct() {
        List<WebElement> listProducts = driver.findElements(listAllProductsSelector);
        listProducts.get(new Random().nextInt(listProducts.size()-1)+1).click();
    }

    /**
     *
     * Method waits for element becomes clickable.
     */
    public void waitForElementClickable(By by) {
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(by));
    }

    /**
     *
     * Method waits for element becomes visible
     */
    public void waitForElementVisible(By by) {
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }

    /**
     *
     * Method opens for particular web page.
     * @param url
     */
    public void open(String url) {
        driver.get(url);
    }

    /**
     *
     * Method returns boolean if element is displayed on the page.
     * @param by
     */
    public boolean canSee(By by) {
        return driver.findElement(by).isDisplayed();
    }

    public void openAllProductPage() {
        driver.findElement(allProductsSelector).click();
    }

    public void addToCart() {
        saveProductData();
        driver.findElement(addToCartButtonSelector).click();
        switchToPopUp();
        driver.findElement(proceedPurchaseButtonSelector).click();
    }

    public void saveProductData() {
        CustomReporter.logAction("Get information about currently opened product");
        String productName = driver.findElement(productNameSelector).getText();
        float productPrice = DataConverter.parsePriceValue(driver.findElement(productPriceSelector).getText());
        if (driver.findElement(productDetailSelector).isDisplayed()) {
            driver.findElement(productDetailSelector).click();
            waitForElementVisible(productQtySelector);
        }
        int quantity = DataConverter.parseStockValue(driver.findElement(productQtySelector).getText());
        String url = driver.getCurrentUrl();
        ProductData.createInstance(productName, quantity, productPrice, url);
    }

    public String getProductName() {
        return driver.findElement(productNameInCartSelector).getAttribute("alt");
    }

    public String getProductPrice() {
        return DataConverter.convertPriceValue(DataConverter.parsePriceValue
                (driver.findElement(productPriceInCartSelector).getText()));
    }

    public String getProductQty() {
        return driver.findElement(productQtyInCartSelector).getAttribute("value");
    }

    public void switchToPopUp(){
        waitForElementClickable(proceedPurchaseButtonSelector);
        String subWindowHandler = null;
        Set<String> handles = driver.getWindowHandles();
        Iterator<String> iterator = handles.iterator();
        while (iterator.hasNext()){
            subWindowHandler = iterator.next();
        }
        driver.switchTo().window(subWindowHandler);
    }

    public void createOrder() {
        UserData userData = new UserData();
        driver.findElement(createOrderButtonSelector).click();
        driver.findElement(inputUserNameSelector).sendKeys(userData.getFirstName());
        driver.findElement(inputUserLastNameSelector).sendKeys(userData.getLastName());
        driver.findElement(inputUserEmailSelector).sendKeys(userData.getEmail());
        driver.findElement(continueButtonSelector).click();
        driver.findElement(inputUserAddressSelector).sendKeys(userData.getAddress());
        driver.findElement(inputUserZipSelector).sendKeys(userData.getZip());
        driver.findElement(inputUserCitySelector).sendKeys(userData.getCity());
        driver.findElement(confirmAddressButtonSelector).click();
        driver.findElement(confirmCarrierSelector).click();
        driver.findElement(paymentMethodCheckboxSelector).click();
        driver.findElement(termsConditionsSelector).click();
        driver.findElement(finishOrderCreationButtonSelector).click();
    }

    public String getText(By by){
       return driver.findElement(by).getText().substring(1);
    }

    public String getProductPriceOnConfirmPage() {
        return DataConverter.convertPriceValue(DataConverter.parsePriceValue
                (driver.findElement(productPriceInCartOnConfirmPageSelector).getText()));
    }

    public String getProductQtyOnConfirmPage() {
        return driver.findElement(productQtyInCartOnConfirmPageSelector).getText();
    }

    public boolean verifyProductQty() {
        if(driver.findElement(productDetailSelector).isDisplayed()){
            driver.findElement(productDetailSelector).click();
            waitForElementVisible(productQtySelector);
        }
        int quantityAfterPurchase = DataConverter.parseStockValue(driver.findElement(productQtySelector).getText());
        int quantityBeforePurchase = ProductData.getInstance().getQty();
        return quantityBeforePurchase - 1 == quantityAfterPurchase;
    }
}