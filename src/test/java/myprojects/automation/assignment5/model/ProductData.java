package myprojects.automation.assignment5.model;

import java.util.Random;

/**
 * Hold Product information that is used among tests.
 */
public class ProductData {
    private static ProductData productDataInstance = null;
    private String name;
    private int qty;
    private float price;
    private String url;

    private ProductData(String name, int qty, float price, String url) {
        this.name = name;
        this.qty = qty;
        this.price = price;
        this.url = url;
    }

    public static ProductData getInstance() {
           return productDataInstance;
    }

    public static ProductData createInstance(String name, int qty, float price, String url) {
            if(productDataInstance == null){
            productDataInstance = new ProductData(name, qty, price, url);
        }
        return productDataInstance;
    }

    public String getName() {
        return name;
    }

    public int getQty() {
        return qty;
    }

    public float getPrice() {
        return price;
    }
    public String getUrl() {
        return url;
    }

    /**
     * @return New Product object with random name, quantity and price values.
     */
    public static ProductData generate() {
        Random random = new Random();
        return new ProductData(
                "New Product " + System.currentTimeMillis(),
                random.nextInt(100) + 1,
                (float) Math.round(random.nextInt(100_00) + 1) / 100, "");
    }

    public static void deleteInstance(){
        productDataInstance = null;
    }
}
