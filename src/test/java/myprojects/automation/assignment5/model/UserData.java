package myprojects.automation.assignment5.model;

import java.util.Random;

public class UserData {

    private String firstName = "Bill";
    private String lastName = "Smith";
    private String address = "Gagarina 1,ave.";
    private String zip = "12555";
    private String city = "Kiev";
    private String email = "email" + System.currentTimeMillis() + "@mail.com";

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() { return email;}

    public String getAddress() { return address; }

    public String getZip() { return zip; }

    public String getCity() { return city; }

}
